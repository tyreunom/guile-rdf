;;;; Copyright (C) 2020 Julien Lepiller <julien@lepiller.eu>
;;;; 
;;;; This library is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU Lesser General Public
;;;; License as published by the Free Software Foundation; either
;;;; version 3 of the License, or (at your option) any later version.
;;;; 
;;;; This library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;;; Lesser General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU Lesser General Public
;;;; License along with this library; if not, write to the Free Software
;;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
;;;; 

(define-module (test-modules testsuite)
  #:export (expected-failures
            turtle-test-manifest
            nquads-test-manifest
            semantics-test-manifest
            manifests))

(define expected-failures
  '())

(define turtle-test-manifest "http://www.w3.org/2013/TurtleTests/manifest.ttl")
(define nquads-test-manifest "http://www.w3.org/2013/N-QuadsTests/manifest.ttl")
(define semantics-test-manifest "https://www.w3.org/2013/rdf-mt-tests/manifest.ttl")
(define manifests
  (list turtle-test-manifest nquads-test-manifest semantics-test-manifest))
